# HugoCommit.sh

## The archaic way

Assume, you’ve got a [Hugo](https://gohugo.io) website. In order to publish an update to [Codeberg Pages](https://codeberg.page) for example, you have to

- run hugo
- Git-commit your changes in the Hugo repo
- Git-push the Hugo repo to Codeberg
- Git-commit your changes in the `page` repo
- Git-push the `page` repo to Codeberg Pages

```
$ hugo
$ hugo serve
$ git add .
$ git commit
$ git push origin master
$ cd ../../page
$ git add .
$ git commit
$ git push origin master
```

## The modern way

`HugoCommit.sh` does all that automatically for you! You just have to execute the script in the command line and let the magic happen!

Build your Hugo: `$ bash HugoCommit.sh`<br>
Live demo: `$ bash HugoCommit.sh serve`<br>
Commit and push: `$ bash HugoCommit.sh commit`

## Setup

1. Set an `outputDir` in your Hugo configuration file. Currently, it must be named `config.yaml`.
2. In the output directory, initialise a Git repository.
3. Copy `HugoCommit.sh` and `commit.sh` to your Hugo project folder.
4. Install `trash-cli` and `coreutils`: `$ sudo apt-get install trash-cli coreutils`

Your project’s file structure should look something like this:

```
<Website project folder>
  │
  ├─ <Main Git repo>
  │    │
  │    ├─ <Hugo project folder>
  │    │    │
  │    │    ├─ archetypes
  │    │    ├─ assets
  │    │    ├─ content
  │    │    ├─ data
  │    │    ├─ layouts
  │    │    ├─ themes
  │    │    │
  │    │    ├─ config.yaml
  │    │    ├─ HugoCommit.sh
  │    │    └─ commit.sh
  │    │
  │    └─ .git
  │
  └─ <PublishDir Git repo>
       └─ .git
```

## Licence

You may freely use HugoCommit’s source code according the [MIT licence](https://codeberg.org/pixelcode/HugoCommit.sh/src/branch/master/LICENCE.md) (slightly modified). A link to my repo as the original source would be great 😎